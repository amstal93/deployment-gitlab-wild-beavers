locals {
  groups = {
    terraform = {
      path                    = "terraform"
      description             = ""
      parent_id               = data.gitlab_group.wild_beavers.id
      project_creation_level  = "developer"
      request_access_enabled  = true
      subgroup_creation_level = "maintainer"
    }
    pipelines = {
      path                    = "pipelines"
      description             = ""
      parent_id               = data.gitlab_group.wild_beavers.id
      project_creation_level  = "developer"
      request_access_enabled  = true
      subgroup_creation_level = "maintainer"
    }
    docker = {
      path                    = "docker"
      description             = ""
      parent_id               = data.gitlab_group.wild_beavers.id
      project_creation_level  = "developer"
      request_access_enabled  = true
      subgroup_creation_level = "maintainer"
    }
    templates = {
      path                    = "templates"
      description             = ""
      parent_id               = data.gitlab_group.wild_beavers.id
      project_creation_level  = "developer"
      request_access_enabled  = true
      subgroup_creation_level = "maintainer"
    }
  }
}
