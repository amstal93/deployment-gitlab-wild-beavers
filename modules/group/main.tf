#####
# Gitlab Group
#####

resource "gitlab_group" "this" {
  name                              = var.name
  path                              = var.path
  description                       = var.description
  lfs_enabled                       = var.lfs_enabled
  request_access_enabled            = var.request_access_enabled
  visibility_level                  = var.visibility_level
  share_with_group_lock             = var.share_with_group_lock
  project_creation_level            = var.project_creation_level
  auto_devops_enabled               = var.auto_devops_enabled
  emails_disabled                   = var.emails_disabled
  mentions_disabled                 = var.mentions_disabled
  subgroup_creation_level           = var.subgroup_creation_level
  require_two_factor_authentication = var.require_two_factor_authentication
  two_factor_grace_period           = var.two_factor_grace_period
  parent_id                         = var.parent_id

  lifecycle {
    prevent_destroy = true
  }
}

resource "gitlab_group_membership" "this" {
  for_each = var.group_memberships

  group_id     = gitlab_group.this.id
  user_id      = lookup(each.value, "user_id")
  access_level = lookup(each.value, "access_level", "maintainer")
  expires_at   = lookup(each.value, "expires_at", null)
}
